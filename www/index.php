<?php
require_once __DIR__ . '/../config.php';
require_once __DIR__ . '/../code/func.php';

list($namespace, $controller, $action) = getSca($_CONFIG['space']['default'], trim($_GET['s'] ?? ''));
if (!in_array($namespace, $_CONFIG['space']['open'])) exit();

define('SPACE', $namespace);
define('CONTROLLER', $controller);
define('ACTION', $action);

//new
$newClass = '\\Controller\\' . ($namespace ? ucwords($namespace) . '\\' : '')  . ucwords($controller);
if (class_exists($newClass)) {
    $newController = new $newClass();
    if (method_exists($newController, $action)) {
        $newController->$action();
    }
}

function getSca($default, $s) {
    $scaArray = explode('/', trim($s));
    $scaArray = array_merge(array_filter($scaArray, function ($v) {
        return $v;
    }));
    $sca = '';
    switch (count($scaArray)) {
        case 1:
            $sca = $default . '/' . $scaArray[0] . '/index';
            break;
        case 2:
            $sca = $default . '/' . implode('/', $scaArray);
            break;
        case 3:
            $sca = implode('/', $scaArray);
            break;
        default:
            $sca = $default . '/index/index';
            break;
    }
    return explode('/', $sca);
}

<?php

namespace Model;

use Daiyong\Db as db;

class Article extends Common {
    public $table = 'article';

    public function rule() {
        return array(
            'title|must' => array(
                array('string', '标题必须在5~64个字符之间', 5, 64),
                array('only', '标题与已有数据重复,请修改', $this->table),
            ),
            'tid|must' => array('idInTable', '类型未填写或输入错误', 'article_type'),
            'images' => array('array', '请传入正确的图片列表')
        );
    }
    public function find($param) {
        $data = toArray(db::find($this->table, $param));
        if ($data['id']) {
            $data['images'] = toArray(@json_decode($data['images'], true));
            $body = db::find('article_body|body', ['aid' => $data['id']]);
            $data['body'] = $body ?? ''; //body可能为undefined,null等这里作空处理
            //类型
            $typeModel = new \Model\ArticleType();
            $data['type'] = $typeModel->find(['id' => $data['tid']]);
        }
        return $data;
    }
    public function findAll($param = [], $orderBy = 'order by id desc', $limit = '') {
        $param = $this->findClear($param);
        $list = db::findAll($this->table, $param, $orderBy . ' ' . $limit);
        //加入类别
        $newType = new \Model\ArticleType();
        $type = setKey($newType->findAll());
        foreach ($list as $k => $v) {
            $list[$k]['images'] = toArray(@json_decode($v['images'], true));
            $list[$k]['type'] = toArray(@$type[$v['tid']]);
        }
        return $list;
    }
    public function export($param = [], $orderBy = '') {
        //查询分类信息
        $newType = new \Model\ArticleType();
        $type = $newType->findAll();
        //处理数据
        $title = ['id', '类型', '标题'];
        $filePath = $this->exportExcel($title, function ($limit) use ($param, $orderBy, $type) {
            $list = $this->findAll($param, $orderBy, $limit);
            foreach ($list as $k => $v) {
                //行内容设置与$title一一对应
                $list[$k] = array($v['id'], $type[$v['tid']]['names'], $v['title']);
            }
            return $list;
        });
        return $filePath;
    }
    public function edit($data) {
        //数据验证
        $verify = $this->verify($data, $this->rule());
        if ($verify !== true) {
            return $this->error($verify);
        }
        $data = $this->auto($data); //加入或去除一些自动生成的字段
        //附表数据
        $dataBody = [
            'body' => $data['body']
        ];
        unset($data['body']);
        //主表操作
        if ($data['id']) {
            $return = db::update($this->table, $data, ['id' => $data['id']]);
            $dataBody['aid'] = $data['id'];
        } else {
            $return = db::insert($this->table, $data);
            $dataBody['aid'] = $return;
        }
        //附表操作
        $id = db::find('article_body|id', ['aid' => $dataBody['aid']]);
        if ($id) {
            $returnBody = db::update('article_body', $dataBody, ['id' => $id]);
        } else {
            $returnBody = db::insert('article_body', $dataBody);
        }
        //返回
        if ($return || $returnBody) return true;
        else return false;
    }
    public function delete($param) {
        if (!$param['id']) return $this->error('为避免错误请传入id删除');
        //查询相关信息
        $data = db::find($this->table, $param);
        if ($data['id']) {
            //删除附表
            db::delete('article_body', ['aid' => $data['id']]);
        }
        //开始删除
        return db::delete($this->table, $param);
    }
    private function auto($data) {
        if ($data['id']) {
            unset($data['time_create']);
            unset($data['date_create']);
        } else {
            $data['time_create'] = date('Y-m-d H:i:s');
            $data['date_create'] = date('Y-m-d');
        }
        if ($data['images']) {
            $data['images'] = json_encode($data['images']);
        }
        return $data;
    }
}

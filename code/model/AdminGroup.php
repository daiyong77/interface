<?php

namespace Model;

use Daiyong\Db as db;

class AdminGroup extends Common {
    public $table = 'admin_group';

    public function rule() {
        return array(
            'name|must' => array(
                array('string', '管理组名称必须在2~10个字符之间', 2, 10),
                array('only', '管理组名称与已有数据重复,请修改', $this->table),
            )
        );
    }
    public function find($param) {
        $data = toArray(db::find($this->table, $param));
        if ($data['id']) {
            $data['power'] = toArray(@json_decode($data['power'], true));
        }
        return $data;
    }
    public function findAll($param = [], $orderBy = 'order by id desc', $limit = 'limit 0,1000') {
        $param = $this->findClear($param);
        $list = db::findAll($this->table, $param, $orderBy . ' ' . $limit);
        //解析power
        foreach ($list as $k => $v) {
            $list[$k]['power'] = toArray(@json_decode($v['power'], true));
        }
        return $list;
    }
    public function export($param = [], $orderBy = '') {
        $title =  ['id', '管理组名称', '拥有权限'];
        $filePath = $this->exportExcel($title, function ($limit) use ($param, $orderBy) {
            $list = $this->findAll($param, $orderBy, $limit);
            foreach ($list as $k => $v) {
                //行内容设置与$title一一对应
                $list[$k] = array($v['id'], $v['name'], implode(',', $v['power']));
            }
            return $list;
        });
        return $filePath;
    }
    public function edit($data) {
        $verify = $this->verify($data, $this->rule());
        if ($verify !== true) {
            return $this->error($verify);
        }
        $data = $this->auto($data); //加入或去除一些自动生成的字段
        if ($data['id']) {
            return db::update($this->table, $data, ['id' => $data['id']]);
        } else {
            return db::insert($this->table, $data);
        }
    }
    public function delete($param) {
        if (!$param['id']) return $this->error('为避免错误请传入id删除');
        //相关联信息判断
        $data = db::find($this->table, $param);
        if ($data['id']) {
            $check = db::find('admin', ['gid' => $data['id']]);
            if ($check) {
                return $this->error('该管理组下还存在管理员,暂时不允许删除,如需删除请先修改或删除组下管理员');
            }
        }
        //开始删除
        return db::delete($this->table, $param);
    }
    private function auto($data) {
        if ($data['power']) {
            $data['power'] = json_encode($data['power']);
        }
        return $data;
    }
}

<?php

namespace Controller\Interface;

class PhoneCode extends \Controller\Common {
    public function index() {
        $post = array(
            'phone' => $_POST['phone']
        );
        if (!isPhone($post['phone'])) $this->error('手机号码格式错误');
        $data =  json_encode([ //请替换第三方接口
            'status' => 1,
            'code' => '123456'
        ]);
        $data = json_decode($data, true);
        if ($data['status'] == 1) {
            $time = time();
            $this->success([
                'codeTime' => $time,
                'codeMd5' => md5($data['code'] . $time . $this->ciphertext) //验证码密文
            ], '验证码发送成功');
        } else {
            $this->success('验证码发送失败请稍后再试');
        }
    }
}

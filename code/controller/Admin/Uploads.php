<?php

namespace Controller\Admin;

class Uploads extends Common {
    public function image() {
        $image = new \Controller\Uploads\Image();
        $image->index();
    }
    public function video() {
        $video = new \Controller\Uploads\Video();
        $video->index();
    }
}

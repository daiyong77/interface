<?php

namespace Controller\Admin;

use Daiyong\File as file;
use Daiyong\Db as db;

class AdminGroup extends Common {
    private $model;
    public function __construct() {
        parent::__construct();
        $this->model = new \Model\AdminGroup();
    }
    public function info() {
        $get = @array(
            'id' => (int)$_GET['id']
        );
        $data = $this->model->find(['id' => $get['id']]);
        //获取所有权限
        $data['allPower'] = $this->getActions();

        $this->success($data);
    }
    private function listSearch() {
        $query = @array(
            'name|like' => '%' . $_GET['name'] . '%',
        );
        $orderBy = $this->sqlOrderBy(['id']); //设置允许用户手动排序的字段
        return [$query, $orderBy];
    }
    public function list() {
        list($query, $orderBy) = $this->listSearch();
        $list = $this->model->findAll($query, $orderBy);
        //查询总数
        $total = (int)db::find($this->model->table . '|count("id")', $this->model->findClear($query));

        $this->success([
            'list' => $list,
            'total' => $total
        ]);
    }
    public function listExport() {
        list($query, $orderBy) = $this->listSearch();
        $filePath = $this->model->export($query, $orderBy);
        $this->download($filePath);
    }
    public function edit() {
        $data = @array(
            'id' => (int)$_POST['id'],
            'name' => $_POST['name'],
            'power' => $_POST['power'],
        );
        $return = $this->model->edit($data);
        if (is_array($return)) {
            $this->error($return['message']);
        }
        if ($data['id']) {
            if ($return) $this->success('修改成功');
            else $this->echo(2, '未进行任何变更');
        } else {
            $this->result($return, '新增成功', '新增失败');
        }
    }
    public function delete() {
        $post = @array(
            'ids' =>  array_map('intval', toArray($_POST['ids']))
        );
        $returns = array();
        foreach ($post['ids'] as $v) {
            $returns[] = array(
                'key' => $v,
                'option' => '删除',
                'return' => $this->model->delete(['id' => $v])
            );
        }
        if (!$returns) $this->error('删除失败');
        $this->resultArray($returns);
    }
    //获取SPACE下所有action
    private function getActions() {
        $dir = 'code/controller/' . lcfirst(SPACE) . '/';
        $fileList = file::treeToList(file::tree($dir, ['Common.php', 'Auth.php']));
        $actionAll = [];
        foreach ($fileList as $v) {
            $action = $this->getAction($dir, $v);
            $actionAll = array_merge($actionAll, $action);
        }
        return $actionAll;
    }
    private function getAction($dir, $file) {
        $controller = preg_replace('/\.php$/', '', lcfirst($file));
        $fileContent = file::get($dir . $file);
        preg_match_all('/public([ ]+)function([ ]+)([^__][\w]+)\(/', $fileContent, $matches);
        $action = [];
        foreach ($matches[3] as $v) {
            $action[] = $controller . '/' . $v;
        }
        return $action;
    }
}

<?php

namespace Controller\Admin;

class Auth extends Common {
    //登陆
    public function login() {
        $post = @array(
            'phone' => $_POST['phone'],
            'username' => $_POST['username'],
            'password' => $_POST['password'],
            'code' => $_POST['code'],
            'codeTime' => $_POST['codeTime'], //验证码时间
            'codeMd5' => $_POST['codeMd5'], //验证码密文
            'codeVerify' =>  md5($_POST['code'] . $_POST['codeTime'] . $this->ciphertext)
        );
        $this->loginVerify($post);
        $adminModel = new \Model\Admin();
        $data = $adminModel->login($post);
        if ($data['id']) {
            $this->success($data, '登陆成功');
        } else {
            $this->error($data['message']);
        }
    }
    private function loginVerify($post) {
        if (
            ($post['username'] && $post['password']) || //用户名密码登陆
            ($post['phone'] && $post['password']) || //手机号密码登陆
            ($post['phone'] && $post['code'] && $post['codeTime'] && $post['codeMd5']) //手机号验证码登陆
        ) {
            if ($post['phone'] && !isPhone($post['phone'])) $this->error('手机号码格式错误');
        } else {
            $this->error('登陆信息不完整');
        }
    }
    //手机验证码
    public function phoneCode() {
        $phoneCode = new \Controller\Interface\PhoneCode();
        $phoneCode->index();
    }
    //退出登陆
    public function loginOut() {
        if (strlen($_SERVER['HTTP_AUTHORIZATION']) == 32) {
            $adminModel = new \Model\Admin();
            $loginOut = $adminModel->loginOut($_SERVER['HTTP_AUTHORIZATION']);
            if ($loginOut) {
                $this->success('服务器退出成功');
            }
        }
        $this->error('服务器退出失败');
    }

    // 文章类型列表
    public function articleTypeList() {
        $controller = new ArticleType();
        $controller->list();
    }
    // 管理员组列表
    public function adminGroupList() {
        $controller = new AdminGroup();
        $controller->list();
    }
    // 用户组列表
    public function userGroupList() {
        $controller = new UserGroup();
        $controller->list();
    }
    //查询自己的信息
    public function myInfo() {
        $controller = new Admin();
        $_GET['id'] = $this->loginUser['id'];
        $controller->info();
    }
    //修改自己的信息
    public function editMyInfo() {
        $controller = new Admin();
        $_POST['id'] = $this->loginUser['id'];
        $controller->edit();
    }
}

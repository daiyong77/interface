<?php

namespace Controller\Admin;

use Daiyong\Db as db;

class Article extends Common {
    private $model;
    public function __construct() {
        parent::__construct();
        $this->model = new \Model\Article();
    }
    public function info() {
        $get = @array(
            'id' => (int)$_GET['id']
        );
        $data = $this->model->find(['id' => $get['id']]);
        $this->success($data);
    }
    private function listSearch() {
        $query = @array(
            'title|like' => '%' . $_GET['title'] . '%',
            'tid' => is_numeric($_GET['tid']) ? $_GET['tid'] : '',
            'date_create|>=' => $_GET['dstart'],
            'date_create|<=' => $_GET['dend']
        );
        $orderBy = $this->sqlOrderBy(['id', 'time_create']); //设置允许用户手动排序的字段
        return [$query, $orderBy];
    }
    public function list() {
        list($query, $orderBy) = $this->listSearch();
        $list = $this->model->findAll($query, $orderBy, $this->sqlLimit());
        //查询总数
        $total = (int)db::find($this->model->table . '|count("id")', $this->model->findClear($query));

        $this->success([
            'list' => $list,
            'total' => $total
        ]);
    }
    public function listExport() {
        list($query, $orderBy) = $this->listSearch();
        $filePath = $this->model->export($query, $orderBy);
        $this->download($filePath);
    }
    public function edit() {
        $data = @array(
            'id' => (int)$_POST['id'],
            'title' => $_POST['title'],
            'description' => $_POST['description'],
            'images' => $_POST['images'],
            'tid' => (int)$_POST['tid'],
            'body' => $_POST['body']
        );
        $return = $this->model->edit($data);
        if (is_array($return)) {
            $this->error($return['message']);
        }
        if ($data['id']) {
            if ($return) $this->success('修改成功');
            else $this->echo(2, '未进行任何变更');
        } else {
            $this->result($return, '新增成功', '新增失败');
        }
    }
    public function delete() {
        $post = @array(
            'ids' =>  array_map('intval', toArray($_POST['ids']))
        );
        $returns = array();
        foreach ($post['ids'] as $v) {
            $returns[] = array(
                'key' => 'id:' . $v,
                'option' => '删除',
                'return' => $this->model->delete(['id' => $v])
            );
        }
        if (!$returns) $this->error('删除失败');
        $this->resultArray($returns);
    }
}

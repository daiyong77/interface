<?php
/*
 * @Author: 代勇 1031850847@qq.com
 * @Date: 2023-11-06 08:53:01
 * @LastEditors: 代勇 1031850847@qq.com
 * @LastEditTime: 2024-03-27 09:30:07
 * @Description: 视频上传
 */

namespace Controller\Uploads;

use \Daiyong\Func;
use \Daiyong\File;

class Video extends \Controller\Common {
    private $savePath = 'www/data/videos/'; //保存的目录
    private $httpPath = 'data/videos/'; //站点访问根目录
    public function index() {
        set_time_limit(0); //视频上传设置无超时时间
        //判断视频格式
        $file = current($_FILES);
        $ext = explode('.', $file['name']);
        $ext = strtolower($ext[count($ext) - 1]);
        //判断允许的条件
        if (!in_array($ext, $this->config['upload']['video']['ext'])) {
            $this->error('"' . $file['name'] . '" 格式错误,请上传(' . implode(',', $this->config['upload']['video']['ext']) . ')格式的视频');
        }
        if ($file['size'] > $this->config['upload']['video']['maxSize']) {
            $maxSize = number_format($this->config['upload']['video']['maxSize'] / 1024 / 1024, 2) * 100 / 100;
            $nowSize = number_format($file['size'] / 1024 / 1024, 2) * 100 / 100;
            $this->error('"' . $file['name'] . '" 文件大小为' . $nowSize . 'M, 超过了' . $maxSize . 'M的上传限制。');
        }
        //设置视频存储路径并保存
        $url = date('Ymd') . '/' . date('YmdHis') . '_' . func::random(5) . '.' . $ext;
        $path = file::path($this->savePath . $url, true);
        if (!move_uploaded_file($file['tmp_name'], $path)) {
            $this->error('上传失败');
        }
        //获取网络路径
        $http = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
        $http_plus = dirname(parse_url($_SERVER['REQUEST_URI'])['path']) . '/';
        if ($http_plus == '\\/') $http_plus = '/';
        $http = $http . $http_plus;

        $this->success(array(
            'name' => $file['name'],
            'url' => $http . $this->httpPath . $url,
        ), '上传成功');
    }
}

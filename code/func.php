<?php
//将数组里面的某个字段作为key
function setKey($array, $key = 'id') {
    $newArray = [];
    foreach ($array as $v) {
        $newArray[$v[$key]] = $v;
    }
    return $newArray;
}
//将字段强制转换成array
function toArray($array) {
    if (!is_array($array)) return [];
    else return $array;
}
//判断字符串格式
function isMail($value) { //是否是邮箱
    return preg_match('/^[\w]+@[\w]+(\.[\w]+){1,}$/', $value);
}
function isPhone($value) { //是否是手机
    return preg_match('/^1[\d]{10}$/', $value);
}
function isDate($value) { //2024-01-23,2024/1/31
    return preg_match('/^[\d]{4}(\-|\/)([0-1]|)[\d](\-|\/)([0-3]|)[\d]$/', $value);
}
function isTime($value) { //2024-01-23 01:23:34,2024/1/31 1:3:4
    return preg_match('/^[\d]{4}(\-|\/)([0-1]|)[\d](\-|\/)([0-3]|)[\d] ([0-2]|)[\d]:([0-5]|)[\d]:([0-5]|)[\d]$/', $value);
}

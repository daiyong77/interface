# 安装方式
composer create-project daiyong/interface

# 目录结构介绍
├── cache #缓存文件夹  
├── code #代码文件夹  
│   ├── cmd #命令行下执行的文件  
│   │   └── test.php #cmd执行  
│   ├── controller  
│   │   ├── Admin #例:admin 命名空间  
│   │   │   └──Article.php #例:admin 命名空间下的控制器  
│   │   └── Common.php #controller 公用类  
│   ├── library #自己写的一些类文件存放地址  
│   └── model  
│   │   ├── Article.php #例:article 模型  
│   │   └── Common.php #model 公用类  
│   ├── config.php #配置文件  
│   └── func.php #通用方法  
├── data #资源存放文件夹  
│   └──mysql #数据库备份存放地址  
│       └──xxx.sql #数据库文件  
└── www #访问入口,外部一切访问从这个文件夹进入  
    ├── data #可访问的文件存放位置  
    │   ├── images #存放图片的文件夹  
    │   └── videos #存放视频的文件夹  
    └── index.php #入口文件  

# 修改数据库数据时,model 类 rule 写法笔记
注:所有表主键必须为 id

```
public function rule(){
    return array(
        '键值' => array(//这是具体说明,如果只有一条可以为一维数组
            array('string', '不满足后的提示信息', 最小长度, 最大长度),
            array('only', '是否唯一', '表名'),
            array('reg', '正则匹配', '正则语句'),
            array('idInTable','该字段值是否对应另一个表的id','另外一个表的表名',['允许不在表里面的id']),
            array('stringName', '1中文计1个字符,3个英文记1个字符,如小于最小长度则1个英文占1个字符', 最小长度, 最大长度),
            array('mail', '请填写正确的邮箱地址'),
            array('array','请传入正确的图片列表'),
            array('phone', '请填写正确的手机号码'),
            array('time','请填写正确的时间'),
            array('date','请填写正确的日期')
            array('int','这个字段必须为数字,且在这个范围内',最小值,最大值),
            array('none','不需要验证但是必须要有的话可以填写这个')
        ),
        'username|must' => array(//这是一个例子,must代表添加或者修改必须不为空
            array('string', '用户名必须在6~15个字符之间', 6, 15),
            array('only', '用户名重复', 'admin'),
            array('reg', '用户名必须为英文或数字组合', '/^[\w]+$/'),
        ),
        'gid'=>array('int','请选择组管理组'),//这是一个例子
        'nickname'=>array('stringName', '昵称必须在2~6个字符之间', 2, 6),//这是一个例子
        'mail'=>array('mail', '请填写正确的邮箱地址'),//这是一个例子
        'group'=>array('idInTable','您选择的类型不存在','admin_type',[0,1])//这是一个例子
    );
}
```

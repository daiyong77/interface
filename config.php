<?php
require_once __DIR__ . '/vendor/autoload.php';

header('Content-type:text/html;charset=utf-8'); //设置编码
date_default_timezone_set('PRC'); //设置时区
set_time_limit(30); //设置页面超时时间,需要更长时间的处理可在对应控制器里面自行取消
ini_set('display_errors', 1); //要隐藏所有错误可将参数改为0或者error_reporting(0);
ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_WARNING); //隐藏一些warning的提示

//开发模式配置
if ($_SERVER['COMPUTERNAME'] == 'DAIYONG') {
    $dbConnect = array(
        'connect' => 'mysql:host=127.0.0.1;dbname=test',
        'username' => 'root',
        'password' =>  'root',
        'charset' => 'utf8'
    );
}

//配置信息
$_CONFIG = array(
    'space' => array(
        'open' => ['admin'], //开启的space(可访问的code/controller/xxx)
        'default' => 'admin', //默认space
    ),
    'db' => $dbConnect ?? array(
        'connect' => 'mysql:host=127.0.0.1;dbname=test',
        'username' => 'root',
        'password' =>  'root',
        'charset' => 'utf8'
    ),
    'page' => array(
        'limitMax' => 300, //用于页面显示数据库查询最大条数
        'limitDefault' => 30 //如传入错误则默认显示的条数
    ),
    'upload' => array( //上传设置
        'image' => array(
            'maxSize' => 5 * 1024 * 1024, //允许的最大大小
            'maxWidth' => 1920, //如上传的图片大于这个宽度则会被压缩到maxWidth
            'ext' => ['jpg', 'png', 'bmp', 'webp'] //允许上传的图片格式
        ),
        'video' => array(
            'maxSize' => 100 * 1024 * 1024, //允许的最大大小
            'ext' => ['mp4'] //允许上传的视频格式
        )
    )
);

// 接口输出status规范说明:
// 0:失败,一般是错误提示
// 1:成功,一般是成功提示
// 2:未进行任何变更,一般是警告提示
// 3:多条处理结果返回,前台酌情处理
// 98:无权限
// 99:未登录
